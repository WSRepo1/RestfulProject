package javabrain.restful2.resources;
import javax.ws.rs.*;
import java.net.URI;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;
import javabrain.restful2.service.UserService2;
import javabrain.restful2.vo.ErrorMessage;
import javabrain.restful2.vo.User;
import javabrain.restful2.vo.UserList;

@Path("/webexusers")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HandlingWebEx
{
	UserService2 userService = new UserService2();
	Response response =	Response.status(Status.NOT_FOUND).entity(new ErrorMessage(500,"Dooc","Not Found")).build();
	@GET
	public Response getUsers()
	{
		UserList userList=userService.getAllUsers();
		if (userList != null)
			return Response.status(Status.FOUND).entity(userList).build();
		else
			throw new ServerErrorException(response);
	}

	@GET
	@Path("/{uname}")
	public Response getUser(@PathParam("uname") String uname)
	{
		User user = userService.getUser(uname);
		if (user != null)
			return Response.status(Status.FOUND).entity(user).build();
		else
			throw new NotFoundException(response);
	}

	@POST
	public Response addUser(@Context UriInfo uriInfo, User user)
	{
		System.out.println("post");
		URI uri = uriInfo.getAbsolutePathBuilder().path(user.getName()).build();
		if (userService.addUser(user) != null)
			return Response.created(uri).build();
		else
		{
			Response response=Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new ErrorMessage(500,"doc","Not allowed to add a user twice")).build();
			throw new WebApplicationException(response);
		}
	}

	@PUT
	@Path("/{uname}")
	public Response editUser(@PathParam("uname") String uname, User user)
	{
		user.setName(uname);
		System.out.println("init Editing a user");
		if (userService.editUser(user) != null)
			return Response.status(Status.ACCEPTED).build();
		else
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();

	}

	@DELETE
	@Path("/{uname}")
	public Response deleteUser(@PathParam("uname") String uname)
	{
		if (userService.deleteUser(uname))
			return Response.status(Status.NOT_FOUND).build();
		else
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}

}