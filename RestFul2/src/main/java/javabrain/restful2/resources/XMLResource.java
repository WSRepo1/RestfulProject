package javabrain.restful2.resources;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javabrain.restful2.service.UserService;
import javabrain.restful2.vo.UserList;

/**
 * XMLResource :
 * Returns XML Response of the User Table
 * 
 * @author chetanl
 * @version RWS14.1
 * **/

@Path("/usersxml")
public class XMLResource
{
	UserService userService= new UserService();
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public UserList getUsers()
	{
		return userService.getAllUsers();
	}
}
