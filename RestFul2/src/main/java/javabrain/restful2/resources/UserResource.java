package javabrain.restful2.resources;

import javax.ws.rs.Consumes;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javabrain.restful2.service.UserService;
import javabrain.restful2.vo.User;
import javabrain.restful2.vo.UserList;

/*******************************************************************************
 * UserResource : Returns JSON Response of the User Table. Method level
 * Produces/Consumes annotations are used. 
 * 
 * @author chetanl
 * @version RWS12.1
 * 
 ****************************************************************************/
@Path("/users")
public class UserResource
{
	UserService userService= new UserService();
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public UserList getUsers()
	{
		return userService.getAllUsers();
	}
	
	@GET
	@Path("/{uname}")
	@Produces(MediaType.APPLICATION_JSON)
	public User getUser(@PathParam("uname")String uname)
	{
		return userService.getUser(uname);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public User addUser(User user)
	{
		System.out.println("Initiallizing addition of user");
		return userService.addUser(user);
	}

	@PUT
	@Path("/{uname}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public User editUser(@PathParam("uname")String uname,User user)
	{
		user.setName(uname);
		System.out.println("init Editing a user");
		return userService.editUser(user);
	}
	
	@DELETE
	@Path("/{uname}")
	public void deleteUser(@PathParam("uname")String uname)
	{
		userService.deleteUser(uname);
	}

}