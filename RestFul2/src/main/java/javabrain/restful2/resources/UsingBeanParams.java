package javabrain.restful2.resources;


import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javabrain.restful2.service.UserService;
import javabrain.restful2.vo.User;
import javabrain.restful2.vo.UserList;

/**
 * UsingBeanParam : Returns JSON Response of the User Table Class level
 * Produces/Consumes annotations are used. QueryParams and HeaderParams are
 * supplied by a BeanParam
 * 
 * @author chetanl
 * @version RWS24.1
 **/

@Path("/user5")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UsingBeanParams
{
	UserService userService = new UserService();

	@GET
	public UserList getUsers(@BeanParam BeanParamStore params)
	{
		if (params.getVal().equals("Hello"))
		{
			if (params.getAge() > 0)
			{
				System.out.println("Age param received");
				return userService.getUsersByAgeLessThan(params.getAge());
			}
			return userService.getAllUsers();
		}
		else
			return null;
	}

	@GET
	@Path("/{uname}")
	public User getUser(@PathParam("uname") String uname)
	{
		return userService.getUser(uname);
	}

	@PUT
	@Path("/{uname}")
	public User editUser(@PathParam("uname") String uname, User user)
	{
		user.setName(uname);
		System.out.println("init Editing a user");
		return userService.editUser(user);
	}
}