package javabrain.restful2.resources;
import java.net.URI;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import javax.ws.rs.core.Response.Status;

import javabrain.restful2.service.UserService;
import javabrain.restful2.vo.User;
import javabrain.restful2.vo.UserList;

@Path("/negusers")

@Produces(value={MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
@Consumes(value={MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
public class ContentNego
{
	UserService userService = new UserService();

	@GET
	public Response getUsers()
	{
		UserList userList=userService.getAllUsers();
		if (userList != null)
			return Response.status(Status.FOUND).entity(userList).build();
		else
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		
	}

	@GET
	@Path("/{uname}")
	public Response getUser(@PathParam("uname") String uname)
	{
		User user = userService.getUser(uname);
		if (user != null)
			return Response.status(Status.FOUND).entity(user).build();
		else
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}

	@POST
	public Response addUser(@Context UriInfo uriInfo, User user)
	{
		URI uri = uriInfo.getAbsolutePathBuilder().path(user.getName()).build();
		if (userService.addUser(user) != null)
			return Response.created(uri).build();
		else
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}

	@PUT
	@Path("/{uname}")
	public Response editUser(@PathParam("uname") String uname, User user)
	{
		user.setName(uname);
		System.out.println("init Editing a user");
		if (userService.editUser(user) != null)
			return Response.status(Status.ACCEPTED).build();
		else
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();

	}

	@DELETE
	@Path("/{uname}")
	public Response deleteUser(@PathParam("uname") String uname)
	{
		if (userService.deleteUser(uname))
			return Response.status(Status.NOT_FOUND).build();
		else
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}

}