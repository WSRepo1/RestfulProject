package javabrain.restful2.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javabrain.restful2.vo.ContainingMessage;
import javabrain.restful2.vo.Crops;
import javabrain.restful2.vo.Message;

@Path("/container")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MessageContainer
{
	
	Message message;
	List<Crops> croplist=new ArrayList<Crops>();
	ContainingMessage containingMessage;
	
	
	public MessageContainer()
	{
		message=new Message();
		message.setDate("12/12/1212");
		message.setId(121);
		message.setMessage("Hiee Container");
		message.setUsename("username");
		Crops crop=new Crops("cdfgv","sddfg","dfg", "dfg");
		Crops crop1=new Crops("cdfregv","stgfjddfg","ddsffg", "dfgdsg");
		croplist.add(crop1);
		croplist.add(crop);
		

		
		
		
		
		containingMessage=new ContainingMessage();
		containingMessage.setMessageid(232);
		containingMessage.setMessage(message);
		containingMessage.setCrops(croplist);
		
		
		
	}
	
	@GET
	public ContainingMessage getContainer()
	{
		
		return containingMessage;
		
		
	}
}
