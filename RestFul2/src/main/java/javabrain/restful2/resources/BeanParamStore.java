package javabrain.restful2.resources;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.QueryParam;

public class BeanParamStore
{
	
	private @QueryParam("agelessthan") int age;
	private @HeaderParam("auth") String val;
	/**
	 * @return the age
	 */
	public int getAge()
	{
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age)
	{
		this.age = age;
	}
	/**
	 * @return the val
	 */
	public String getVal()
	{
		return val;
	}
	/**
	 * @param val the val to set
	 */
	public void setVal(String val)
	{
		this.val = val;
	}
	
	
	

}
