package javabrain.restful2.resources;

import java.util.List;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javabrain.restful2.vo.Crops;
import javabrain.restful2.vo.Photo;

@Path("/photoserve")
@Produces(value={MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
@Consumes(MediaType.APPLICATION_JSON)
public class PhotoServe
{
	
	Photo photo=new Photo();
	List<Crops> croplist=new ArrayList<Crops>();
	
	
	
	public PhotoServe()
	{
		
		photo=new Photo();
		photo.setId("id");
		photo.setNamespace("null");
		photo.setSize("Size");
		photo.setUrl("URL");
		photo.setSlug("null");
		Crops crop=new Crops("cdfgv","sddfg","dfg", "dfg");
		Crops crop1=new Crops("cdfregv","stgfjddfg","ddsffg", "dfgdsg");
		croplist.add(crop1);
		croplist.add(crop);
		photo.setCrops(croplist);
	}

	
	
	@GET
	public Photo getPhoto()
	{
		return photo;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addUser(Photo photo)
	{
		System.out.println("Initiallizing Serving photoAccept");
		System.out.println(photo.getId());
		return Response.accepted().build();
		
		
	}

	
	
	
}
