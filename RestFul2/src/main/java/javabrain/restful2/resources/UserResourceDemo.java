package javabrain.restful2.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * UserResourceDemo :
 * Returns Custom Response for miscellaneous operations 
 * 
 * @author chetanl
 * @version RWS17.1
 * **/



@Path("/usersdemo")
public class UserResourceDemo
{
	@GET
	@Path("/test")
	@Produces(MediaType.TEXT_PLAIN)
	public String pathParamTest()
	{
		return "Test of specific Path Param";
	}
	
	@GET
	@Path("/{text}")
	@Produces(MediaType.TEXT_PLAIN)
	public String pathParamTestRandom()
	{
		return "Test of Random Path Param";
	}
}
