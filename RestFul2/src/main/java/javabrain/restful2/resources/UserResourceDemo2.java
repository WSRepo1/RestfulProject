package javabrain.restful2.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


/**
 * UserResourceDemo2 :
 * Returns Custom Response for Miscellaneous Operations 
 * 
 * @author chetanl
 * @version RWS17.2
 * **/



@Path("/usersdemo2")
public class UserResourceDemo2
{

	
	@GET
	@Path("/{text}")
	@Produces(MediaType.TEXT_PLAIN)
	public String pathParamPrintRandom(@PathParam("text")String text)
	{
		return "Hii Random parameter : "+text;
	}
}
