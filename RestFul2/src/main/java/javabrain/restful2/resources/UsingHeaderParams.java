package javabrain.restful2.resources;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import javabrain.restful2.service.UserService;
import javabrain.restful2.vo.User;
import javabrain.restful2.vo.UserList;

/*******************************************************************************
 * UsingHeaderParams : Returns JSON Response of the User Table Class level
 * Produces/Consumes annotations are used. Using Custom Headers for
 * Authentication
 * 
 * @author chetanl
 * @version RWS23.1
 * 
 ****************************************************************************/
@Path("/user4")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UsingHeaderParams
{
	UserService userService = new UserService();

	@GET
	public UserList getUsers(@QueryParam("agelessthan") int age, @HeaderParam("auth") String val)
	{
		if (val.equals("Hello"))
		{
			if (age > 0)
			{
				System.out.println("Age param received");
				return userService.getUsersByAgeLessThan(age);
			}
			return userService.getAllUsers();
		}
		return null;
	}

	@GET
	@Path("/{uname}")
	public User getUser(@PathParam("uname") String uname)
	{
		return userService.getUser(uname);
	}

	@POST
	public User addUser(User user)
	{
		System.out.println("Initiallizing addition of user");
		return userService.addUser(user);
	}

	@PUT
	@Path("/{uname}")
	public User editUser(@PathParam("uname") String uname, User user)
	{
		user.setName(uname);
		System.out.println("init Editing a user");
		return userService.editUser(user);
	}
}