package javabrain.restful2.misc;

import java.io.FileInputStream;
import java.util.Properties;


/**
 * Props :
 * Loads Properties i.e. Database Credentials from a separate properties file
 * located at desktop.
 * 
 * @author chetanlovanshi
 * @version RWS17.1
 * **/

public class Props
{
	public String username, password, port, url, dbname;
	Properties properties;

	public Props()
	{
		properties=new Properties();
		loader();
		System.out.println("Properties Loaded");
	}
	
	
	public void loader()
	{
		try
		{
			String loc = System.getProperty("user.home")+"\\"+"desktop"+"\\"+"prop.properties";
			System.out.println("finding prop at " + loc);
			properties.load(new FileInputStream(loc));
			
			username=properties.getProperty("username");
			System.out.println(username);
			dbname=properties.getProperty("dbname");
			password=properties.getProperty("password");
			url=properties.getProperty("url");
			port=properties.getProperty("port");
			System.out.println("Got all values for authentication");
		}
		catch (Exception e)
		{
			System.out.println("Exception at Props" + e.getMessage());
			e.printStackTrace();
		}
	}
}
