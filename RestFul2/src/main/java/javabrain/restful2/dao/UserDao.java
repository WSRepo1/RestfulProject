
package javabrain.restful2.dao;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import javabrain.restful2.db.DataSource;
import javabrain.restful2.vo.User;
import javabrain.restful2.vo.UserList;

/**
 * UserDao : Communicates with Database for User Related functions
 * 
 * @author chetanl
 * 
 */
public class UserDao
{
	Connection con;
	private UserList userList;
	PreparedStatement ps;
	ResultSet rs;

	public UserDao()
	{
		userList = new UserList();
		con = DataSource.getConnection();
	}

	public int addUser(User user)
	{
		int count = 0;
		try
		{
			System.out.println("Adding a user");
			ps = con.prepareStatement("insert into usertable  values(?,?,?,?)");
			ps.setString(1, user.getName());
			ps.setString(3, user.getPhone());
			ps.setString(4, user.getCity());
			ps.setInt(2, user.getAge());
			count = ps.executeUpdate();
			System.out.println("Rows added" + count);
		}
		catch (Exception e)
		{
			System.out.println("Exception while adding a user: " + e.getMessage());
		}
		return count;
	}

	public UserList getAllUsers()
	{
		try
		{
			ps = con.prepareStatement("select * from usertable ");
			rs = ps.executeQuery();
			if (rs == null)
				return null;
			else
			{
				while (rs.next())
				{
					User user = new User();
					user.setName(rs.getString(1));
					user.setAge(rs.getInt(2));
					user.setPhone(rs.getString(3));
					user.setCity(rs.getString(4));
					userList.getUserList().add(user);
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("Exception in Getting All Users");
			System.out.println(e.getMessage());
		}
		return userList;
	}

	public User getUser(String uname)
	{
		User user = null;
		try
		{
			System.out.println("Finding user" + uname);
			ps = con.prepareStatement("select * from usertable where name=?");
			System.out.println("Got Connection : Fetching Query");
			ps.setString(1, uname);
			rs = ps.executeQuery();
			if (rs.next())
			{
				user = new User();
				user.setAge(rs.getInt(2));
				user.setCity(rs.getString(4));
				user.setName(rs.getString(1));
				user.setPhone(rs.getString(3));
			}

		}
		catch (Exception e)
		{
			System.out.println("Exception while finding user" + uname + ":" + e.getMessage());
			return null;
		}
		return user;
	}

	public boolean editUser(User user)
	{
		boolean result = false;
		String uname = user.getName();
		try
		{
			System.out.println("Finding user" + uname);
			ps = con.prepareStatement("update usertable set age=?,phone=?,city=? where name=?");
			System.out.println("Got Connection : Fetching Query");
			ps.setString(4, uname);
			ps.setString(2, user.getPhone());
			ps.setString(3, user.getCity());
			ps.setInt(1, user.getAge());

			int res = ps.executeUpdate();
			if (res == 1)
				result=true;
		}
		catch (Exception e)
		{
			System.out.println("Exception while Updating user" + uname + ":" + e.getMessage());
		}
		return result;
	}

	public boolean deleteUser(String uname)
	{
		boolean result=false;
		try
		{
			System.out.println("Deleting user" + uname);
			ps = con.prepareStatement("delete from usertable where name=?");
			System.out.println("Got Connection : Executing Query");
			ps.setString(1, uname);
			int res = ps.executeUpdate();
			if (res>0)
				result=true;
		}
		catch (Exception e)
		{
			System.out.println("Exception while Deleting user" + uname + ":" + e.getMessage());
		}
		return result;
	}

	

}