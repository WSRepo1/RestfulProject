package javabrain.restful2.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import javabrain.restful2.vo.ErrorMessage;

@Provider
public class DataNotFoundExceptionMapper implements ExceptionMapper<DataNotFoundException>
{
	public Response toResponse(DataNotFoundException ex)
	{
		ErrorMessage errorMessage=new ErrorMessage(404,ex.getMessage(),"Sample Link"); 
		return Response.status(Status.NOT_FOUND).entity(errorMessage).
				build();
		
		
		//.entity(new )
		
	}

	
}
