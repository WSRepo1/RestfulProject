package javabrain.restful2.exceptions;


import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import javabrain.restful2.vo.ErrorMessage;

//@Provider  // Enable the @provider annotation to register the this exception to the jax-rs
public class GenericExceptionMapper implements ExceptionMapper<Throwable>
{
	public Response toResponse(Throwable ex)
	{
		ErrorMessage errorMessage=new ErrorMessage(500,ex.getMessage(),"Internal Server Error"); 
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(errorMessage).
				build();
		
		
		//.entity(new )
		
	}

	
}
