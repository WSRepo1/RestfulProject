package javabrain.restful2.db;
import java.sql.Connection;
import java.sql.DriverManager;

public class DataSource
{
	public static Connection con;
	public static Connection getConnection()
	{
		if (con == null)
			con = prepareConnection();
		return con;
	}

	private static Connection prepareConnection()
	{
		try
		{
			System.out.println("Derby connection init");
			String driver = "org.apache.derby.jdbc.EmbeddedDriver";
			String protocol = "jdbc:derby:"; 
			Class.forName(driver).newInstance();
			System.out.println("Connection Established");
			con = DriverManager.getConnection(protocol + "MyDbTest;create=true");
			//jdbc:derby:MyDbTest;shutdown=true
		}
		catch (Exception e)
		{
			e.printStackTrace();
			con = null;
		}
		return con;
	}
}