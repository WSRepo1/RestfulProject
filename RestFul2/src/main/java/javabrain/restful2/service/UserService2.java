package javabrain.restful2.service;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import javabrain.restful2.dao.UserDao;
import javabrain.restful2.vo.User;
import javabrain.restful2.vo.UserList;

/**
 * UserService2 :
 * Contains method for Users calling and other operations.
 * Throws WebApplicationException.
 * Fetches data from Database.
 * Same as UserService
 * 
 * @author Soni
 * @version RWS28.0
 * **/

public class UserService2
{
	UserDao userDao = new UserDao();
	private UserList userList = new UserList();
	public UserList getAllUsers()
	{
		userList = userDao.getAllUsers();
		return userList;
	}

	public UserList getUsersByAgeLessThan(int age)
	{
		System.out.println("finding users lesser age than "+age);
		userList = userDao.getAllUsers();
		UserList age_Userlist =new UserList();
		for(User user:userList.getUserList())
		{
			if(user.getAge()<age)
				age_Userlist.getUserList().add(user);
		}
		System.out.println("Users Found :"+age_Userlist.getUserList().size());
		return age_Userlist;
	}
	
	
	
	public User addUser(User user)
	{
		System.out.println("User service : addUser");
		if (userDao.addUser(user) == 1)
			return user;
		else
			return null;
	}

	public User getUser(String uname)
	{
		User user = userDao.getUser(uname);
		if(user==null)
			throw new WebApplicationException(Status.NOT_FOUND);
		return user;
	}

	public User editUser(User user)
	{
		if (userDao.editUser(user))
			return user;
		else
			return null;
	}

	public boolean deleteUser(String uname)
	{
		 return userDao.deleteUser(uname);
	}
	
	
	
	
	
	
	
	
}
