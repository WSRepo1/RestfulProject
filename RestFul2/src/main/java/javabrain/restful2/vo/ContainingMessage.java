package javabrain.restful2.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ContainingMessage
{

	private int messageid;
	private Message message;
	private List<Crops> crops = new ArrayList<Crops>();

	
	
	
	
	
	/**
	 * @return the crops
	 */
	public List<Crops> getCrops()
	{
		return crops;
	}

	/**
	 * @param crops the crops to set
	 */
	public void setCrops(List<Crops> crops)
	{
		this.crops = crops;
	}

	public ContainingMessage()
	{
	}

	/**
	 * @return the messageid
	 */
	public int getMessageid()
	{
		return messageid;
	}

	/**
	 * @param messageid
	 *            the messageid to set
	 */
	public void setMessageid(int messageid)
	{
		this.messageid = messageid;
	}

	/**
	 * @return the message
	 */
	public Message getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(Message message)
	{
		this.message = message;
	}

}
