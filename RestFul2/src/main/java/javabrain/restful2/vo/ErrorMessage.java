package javabrain.restful2.vo;
import javax.xml.bind.annotation.XmlRootElement;

/*******************************************************************************
 * ErrorMessage : Returns JSON Response of the Error/Exception occurance.
 * 
 * @author chetanl
 * @version RWS27.1
 ****************************************************************************/
@XmlRootElement
public class ErrorMessage
{
	
	private int errorCode;
	private String documentation,errorMessage;
	
	public ErrorMessage()
	{}
	
	
	
	public ErrorMessage(int errorCode, String documentation, String errorMessage)
	{
		super();
		this.errorCode = errorCode;
		this.documentation = documentation;
		this.errorMessage = errorMessage;
	}



	/**
	 * @return the errorCode
	 */
	public int getErrorCode()
	{
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(int errorCode)
	{
		this.errorCode = errorCode;
	}
	/**
	 * @return the documentation
	 */
	public String getDocumentation()
	{
		return documentation;
	}
	/**
	 * @param documentation the documentation to set
	 */
	public void setDocumentation(String documentation)
	{
		this.documentation = documentation;
	}
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage()
	{
		return errorMessage;
	}
	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

}
