package javabrain.restful2.vo;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@ XmlRootElement
public class UserList
{
	
	public UserList()
	{}
	
	private ArrayList<User> userList=new ArrayList<User>();

	/**
	 * @return the userList
	 */
	public ArrayList<User> getUserList()
	{
		return userList;
	}

	/**
	 * @param userList the userList to set
	 */
	public void setUserList(ArrayList<User> userList)
	{
		this.userList = userList;
	}
	
	
	
	

}
