package javabrain.restful2.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="Photo")
public class Photo
{
	private String namespace,slug,size,id,url;
	
	private List<Crops> crops = new ArrayList<Crops>();
	/**
	 * @return the namespace
	 */
	public String getNamespace()
	{
		return namespace;
	}
	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(String namespace)
	{
		this.namespace = namespace;
	}
	/**
	 * @return the slug
	 */
	public String getSlug()
	{
		return slug;
	}
	/**
	 * @param slug the slug to set
	 */
	public void setSlug(String slug)
	{
		this.slug = slug;
	}
	/**
	 * @return the size
	 */
	public String getSize()
	{
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(String size)
	{
		this.size = size;
	}
	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id)
	{
		this.id = id;
	}
	/**
	 * @return the url
	 */
	public String getUrl()
	{
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}
	/**
	 * @return the crops
	 */
	public List<Crops> getCrops()
	{
		return crops;
	}
	/**
	 * @param crops the crops to set
	 */
	
	public void setCrops(List<Crops> crops)
	{
		this.crops = crops;
	}
	
	
	
	
	
	

}
