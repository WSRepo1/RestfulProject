package javabrain.restful2.vo;

public class Message
{
	private String usename,date,message;
	private int id;
	/**
	 * @return the usename
	 */
	public String getUsename()
	{
		return usename;
	}
	/**
	 * @param usename the usename to set
	 */
	public void setUsename(String usename)
	{
		this.usename = usename;
	}
	/**
	 * @return the date
	 */
	public String getDate()
	{
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date)
	{
		this.date = date;
	}
	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}
	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}
	

}
