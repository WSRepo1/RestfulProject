package javabrain.restful2.vo;



public class Crops
{
	private String height,width,left,right;
	
	
	
	public Crops()
	{}
	
	public Crops(String height, String width, String left, String right)
	{
		this.height = height;
		this.width = width;
		this.left = left;
		this.right = right;
	}



	/**
	 * @return the height
	 */
	public String getHeight()
	{
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(String height)
	{
		this.height = height;
	}

	/**
	 * @return the width
	 */
	public String getWidth()
	{
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(String width)
	{
		this.width = width;
	}

	/**
	 * @return the left
	 */
	public String getLeft()
	{
		return left;
	}

	/**
	 * @param left the left to set
	 */
	public void setLeft(String left)
	{
		this.left = left;
	}

	/**
	 * @return the right
	 */
	public String getRight()
	{
		return right;
	}

	/**
	 * @param right the right to set
	 */
	public void setRight(String right)
	{
		this.right = right;
	}
}
